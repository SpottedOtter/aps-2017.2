#!/bin/bash
find . | grep -E "(__pycache__|\.pyc|\.pyo$|__init__)" | xargs rm -rf
rm -rf venv
echo "Success!"