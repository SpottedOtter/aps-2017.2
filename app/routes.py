from app import app
from flask import render_template, request, redirect, url_for, session, flash
from app.forms import RegisterForm, LoginForm, CineForm, FilmForm, SessionForm, BuyForm, PasswordRecoveryForm
from app.models import *
from sqlalchemy import exc
from datetime import date
from operator import itemgetter

from app.models import Ingresso

estados = ["Acre (AC)","Alagoas (AL)","Amapá (AP)","Amazonas (AM)","Bahia (BA)","Ceará (CE)",
"Distrito Federal (DF)","Espírito Santo (ES)","Goiás (GO)","Maranhão (MA)","Mato Grosso (MT)",
"Mato Grosso do Sul (MS)","Minas Gerais (MG)","Pará (PA)","Paraíba (PB)","Paraná (PR)",
"Pernambuco (PE)","Piauí (PI)","Rio de Janeiro (RJ)","Rio Grande do Norte (RN)",
"Rio Grande do Sul (RS)","Rondônia (RO)","Roraima (RR)","Santa Catarina (SC)","São Paulo (SP)",
"Sergipe (SE)","Tocantins (TO)"]

def cadastraAdmins():
    adminNum = Admin.query.count()
    if(adminNum == 0):
        admin1 = Admin("darcyfontenele@gmail.com", "darcy123")
        admin2 = Admin("levy@larces.com.br", "levy123")
        admin3 = Admin("admin@admin.com", "admin123")
        db.session.add(admin1)
        db.session.add(admin2)
        db.session.add(admin3)
        db.session.commit()

@app.route('/')
@app.route('/index')
def index():
    cadastraAdmins()
    form = FilmForm(request.form)
    filmes = Filme.query.all()
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            return redirect(url_for("admin"))
        else:
            select = request.form.get('estado-select')
            if request.method == 'POST':
                if select != "None":
                    select = request.form.get('estado-select')
                    filmes = Cinema.query.filter_by(estado=select).join(Sessao,
                                                                        Cinema.id == Sessao.cinema_id).add_columns(
                        Sessao.id, Cinema.id).join(Filme, Sessao.filme_id == Filme.id).add_columns(Filme.id,
                                                                                                   Filme.titulo,
                                                                                                   Filme.classificacao,
                                                                                                   Filme.imagem,
                                                                                                   Filme.genero).group_by(
                        Filme.titulo)
    cadastraAdmins()
    return render_template('index.html', estados=estados, form=form, filmes=filmes)

@app.route('/login', methods= ['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        email = form.email.data
        password = form.password.data
        admin = Admin.query.filter_by(email=email).first()
        user = Cliente.query.filter_by(email=email).first()
        if user is not None:
            if user.senha == password:
                flash("Usuario logado com sucesso", 'success')
                session['logged_in'] = True
                session['email'] = user.email
                session['admin'] = False
                return redirect(url_for("index"))
            else:
                flash("Senha Incorreta", 'danger')
        elif admin is not None:
            if admin.password == password:
                flash("Admin logado com sucesso", "sucess")
                session['logged_in'] = True
                session['email'] = admin.email
                session['admin'] = True
                return redirect(url_for("admin"))
            else:
                flash("Senha Incorreta", 'danger')
        else:
            flash("Usuario nao encontrado", 'danger')
            return render_template("login.html", form=form)
    return render_template('login.html', form=form)

@app.route('/logout')
def logout():
    session.pop('logged_in', False)
    session.pop('email', None)
    session.pop('admin', False)
    return redirect(url_for("index"))

@app.route("/passwordRecovery", methods = ['GET', 'POST'])
def recuperar_senha():
    form = PasswordRecoveryForm(request.form)
    if request.method == 'POST' and form.validate():
        _email = form.email.data
        user = Cliente.query.filter_by(email = _email).first()
        if user is not None:
            flash('Email enviado com sucesso', 'success')
            return redirect(url_for('login'))
        else:
            flash('Email não encontrado', 'danger')
        
    return render_template("passwordRecovery.html", form = form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        password = str(form.password.data)
        cpf = form.cpf.data
        birthday = form.birthday.data
        avatar = form.avatar.data #AINDA NAO TEM O CAMPO PARA IMAGEM NO BD
        isStudent = form.isStudent.data
        admin = Admin.query.filter_by(email=email).first()
        if admin is None:
            cliente = Cliente(name, email, password, cpf, birthday, isStudent)
            try:
                db.session.add(cliente)
                db.session.commit()
                flash('Cadastro Realizado com Sucesso', 'success')
                return redirect(url_for("index"))
            except exc.IntegrityError as e:
                flash('Preencha os Campos Corretamente', 'danger')
                db.session().rollback()
        else:
            flash("Email já cadastrado", 'danger')
    return render_template('register.html', form = form)

@app.route('/alterarcadastro', methods=['GET', 'POST'])
def alterar_cadastro():
    form = RegisterForm(request.form)
    cliente = Cliente.query.filter_by(email=session['email']).first()
    if request.method == 'POST' and form.validate():
        cliente.nome = form.name.data
        cliente.email = form.email.data
        cliente.senha = str(form.password.data)
        cliente.cpf = form.cpf.data
        cliente.dtNasc = form.birthday.data
        cliente.isEstudante = form.isStudent.data
        admin = Admin.query.filter_by(email=cliente.email).first()
        if admin is None:
            try:
                db.session.commit()
                flash('Alteracao Realizada com Sucesso', 'success')
                redirect(url_for("index"))
                return redirect(url_for("index"))
            except exc.IntegrityError as e:
                flash('Preencha os Campos Corretamente', 'danger')
                db.session().rollback()
        else:
            flash("Email já cadastrado", 'danger')
    return render_template('register.html', form = form, cliente=cliente, funcao="editar")

@app.route('/films', methods=['GET', 'POST'])
def films():
    form = FilmForm(request.form)
    filmes = Filme.query.all()
    select = request.form.get('estado-select')
    if request.method == 'POST':
        if select != "None":
            select = request.form.get('estado-select')
            filmes = Cinema.query.filter_by(estado=select).join(Sessao, Cinema.id == Sessao.cinema_id).add_columns(
            Sessao.id, Cinema.id).join(Filme, Sessao.filme_id == Filme.id).add_columns(Filme.id, Filme.titulo, Filme.classificacao,
                                                                                   Filme.imagem, Filme.genero).group_by(Filme.titulo)
    return render_template("films.html", estados = estados, filmes=filmes, form=form, sestado=select)

@app.route('/films/<film_id>/', methods = ['GET', 'POST'])
def single_film(film_id):
    movie = Filme.query.filter_by(id=film_id).first()
    sessoes = Sessao.query.filter_by(filme_id=film_id).join(Cinema, Sessao.cinema_id == Cinema.id).add_columns(
        Sessao.id, Sessao.data, Cinema.nome,
        Cinema.localizacao,
        Sessao.horario)
    sessao = Sessao.query.filter_by(filme_id=film_id).first()
    return render_template("film_template.html", film=movie, sessoes=sessoes)

@app.route('/compradeingresso/<id>', methods = ['GET', 'POST'])
def ticket_purchase(id):
    if 'logged_in' in session:
        admin = session['admin']
        if admin is True:
            flash("É necessario ser cliente para acessar!", 'danger')
            return redirect(url_for("index"))
        else:
            cliente = Cliente.query.filter_by(email=session['email']).first()
            sessao = Sessao.query.filter_by(id=id).join(Cinema, Sessao.cinema_id == Cinema.id).add_columns(
                Sessao.id, Sessao.data, Sessao.preco, Sessao.lotacao, Sessao.cinema_id, Cinema.nome,
                Cinema.localizacao, Sessao.filme_id,
                Sessao.horario).join(Filme, Sessao.filme_id == Filme.id).add_columns(
                Sessao.id, Sessao.data, Sessao.preco, Sessao.lotacao, Sessao.cinema_id, Cinema.nome, Cinema.localizacao, Sessao.filme_id,
                Filme.titulo,
                Filme.classificacao, Sessao.horario)
            sessao = sessao.first()
            qntOcupada = Ingresso.query.filter_by(sessao_id=id).count()
            if request.method == 'POST':
                qnt = int(request.form['ticketqnt'])
                if qnt > 0:
                    try:
                        idade = ((date.today() - cliente.dtNasc)/365).days
                        preco_total = sessao.preco*qnt
                        if (sessao.lotacao-qntOcupada) >= qnt:
                            if int(sessao.classificacao) < idade:
                                if cliente.isEstudante == True:
                                    preco_total = preco_total/2
                                compra = Compra(cliente.id, qnt, preco_total)
                                db.session.add(compra)
                                db.session.commit()
                                for i in range(qnt):
                                    ingresso = Ingresso(sessao.cinema_id, sessao.filme_id, sessao.id, Compra.query.count(),
                                                sessao.preco)
                                    db.session.add(ingresso)
                                    db.session.commit()
                                numero = request.form['numero']
                                option = request.form['options']  # pagamento
                                print(option)
                                parcelas = request.form.get('parcelas')
                                print(str(parcelas))
                                if option != "credito" and str(parcelas) == '2':
                                    if request.form.get("preco-total") < 30:
                                        flash('Parcelamento não permitido. Compra realizada com sucesso', 'sucesso')
                                else:
                                    flash('Compra Realizada com Sucesso', 'success')
                            else:
                                flash('Classificacao não indicada', 'danger')
                        else:
                            flash('Quantidade de ingressos indisponível', 'danger')
                    except exc.IntegrityError as e:
                        flash('Preencha os Campos Corretamente', 'danger')
                        db.session().rollback()
                else:
                    flash('Quantidade de ingressos invalida', 'danger')
                    db.session().rollback()

    else:
        flash("É necessario estar logado para comprar!", 'danger')
        return redirect(url_for("login"))
    return render_template("ticketpurchase.html", sessao=sessao, cliente=cliente)

@app.route('/admin', methods = ['GET', 'POST'])
def admin():
    if 'admin' not in session:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    else:
        admin = session['admin']
        if admin is False:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    return render_template('admin.html')

@app.route('/admin/gerenciarfilmes/cadastrarfilme', methods = ['GET', 'POST'])
def cadastra_filme():
    form = FilmForm(request.form)
    if 'admin' not in session:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    else:
        admin = session['admin']
        if admin is False:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
        else:
            if request.method == 'POST' and form.validate():
                titulo = form.titulo.data
                sinopse = request.form['sinopse']
                genero = form.genero.data
                imagem = form.imagem.data
                classificacao = form.classificacao.data
                filme = Filme(titulo, sinopse, genero, imagem, classificacao)
                try:
                    db.session.add(filme)
                    db.session.commit()
                    flash('Cadastro Realizado com Sucesso', 'success')
                    return redirect(url_for("cadastra_filme"))
                except exc.IntegrityError as e:
                    flash('Preencha os Campos Corretamente', 'danger')
                    db.session().rollback()
    return render_template('filmregister.html', form=form)

@app.route('/admin/gerenciarfilmes/editarfilme/<id>', methods = ['GET', 'POST'])
def edita_filme(id):
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            form = FilmForm(request.form)
            filme = Filme.query.filter_by(id=id).first()
            if request.method == "POST" and form.validate():
                filme.titulo = form.titulo.data
                filme.sinopse = request.form['sinopse']
                filme.genero = form.genero.data
                filme.imagem = form.imagem.data
                filme.classificacao = form.classificacao.data
                try:
                    db.session.commit()
                    flash('Alteracao Realizada com Sucesso', 'success')
                    redirect(url_for("manage_handle"))
                except exc.IntegrityError as e:
                    flash('Preencha os Campos Corretamente', 'danger')
                    db.session().rollback()
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('filmregister.html', form=form, funcao="editar", filme=filme)

@app.route('/admin/gerenciarfilmes/removefilme/<id>', methods = ['GET', 'POST'])
def remove_filme(id):
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            Filme.query.filter_by(id=id).delete()
            Sessao.query.filter_by(filme_id=id).delete()
            db.session.commit()
            return redirect(url_for("index"))
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('index.html')

@app.route('/admin/gerenciarcinemas/cadastrarcinema', methods = ['GET', 'POST'])
def cadastra_cinema():
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            form = CineForm(request.form)
            if request.method == 'POST' and form.validate():
                name = form.name.data
                state = request.form.get('states')
                city = form.city.data
                localization = form.localization.data
                cinema = Cinema(name, state, city, localization)
                try:
                    db.session.add(cinema)
                    db.session.commit()
                    flash('Cadastro Realizado com Sucesso', 'success')
                    redirect(url_for("manage_handle"))
                except exc.IntegrityError as e:
                    flash('Preencha os Campos Corretamente', 'danger')
                    db.session().rollback()
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('cineregister.html', estados=estados, form=form)

@app.route('/admin/gerenciarcinemas/editarcinema/<id>', methods = ['GET', 'POST'])
def edita_cinema(id):
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            form = CineForm(request.form)
            cinema = Cinema.query.filter_by(id=id).first()
            if request.method == "POST" and form.validate():
                cinema.nome = form.name.data
                cinema.estado = request.form.get('states')
                cinema.cidade = form.city.data
                cinema.localizacao = form.localization.data
                try:
                    db.session.commit()
                    flash('Alteracao Realizada com Sucesso', 'success')
                    redirect(url_for("manage_handle"))
                except exc.IntegrityError as e:
                    flash('Preencha os Campos Corretamente', 'danger')
                    db.session().rollback()
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('cineregister.html', form=form, estados=estados, funcao="editar", cinema=cinema)

@app.route('/admin/gerenciarcinemas/removecinema/<id>', methods = ['GET', 'POST'])
def remove_cinema(id):
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            Cinema.query.filter_by(id=id).delete()
            Sessao.query.filter_by(cinema_id=id).delete()
            db.session.commit()
            return redirect(url_for("index"))
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('index.html')

@app.route('/admin/gerenciar', methods = ['GET', 'POST'])
def manage_handle():
    if request.method == 'GET':
        option = request.args.get("option")
        if option == "filmes":
            filmes = Filme.query.all()
            return render_template("manage.html", url="/admin/gerenciarfilmes/cadastrarfilme"
                                   , valores=filmes, classe="Filme")
        if option == "cinemas":
            cinemas = Cinema.query.order_by(Cinema.nome).all()
            return render_template("manage.html", url="/admin/gerenciarcinemas/cadastrarcinema"
                                   , valores=cinemas, classe="Cinema")
        if option == "sessoes":
            sessoes = Sessao.query.join(Cinema, Sessao.cinema_id == Cinema.id).add_columns(Sessao.id, Cinema.nome,
                                        Sessao.filme_id, Sessao.horario).join(Filme, Sessao.filme_id == Filme.id).add_columns(
                                        Sessao.id, Cinema.nome, Filme.titulo, Sessao.horario)
            return render_template("manage.html", url="/admin/gerenciarsessoes/cadastrarsessao"
                                   , valores=sessoes, classe="Sessao")

@app.route('/admin/gerenciarsessoes/cadastrarsessao', methods = ['GET', 'POST'])
def cadastra_sessao():
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            form = SessionForm(request.form)
            filmes = Filme.query.all()
            cinemas = Cinema.query.all()
            if request.method == 'POST' and form.validate():
                cinema = request.form.get('cines')
                filme = request.form.get('films')
                cine = Cinema.query.filter_by(nome=cinema).first()
                film = Filme.query.filter_by(titulo=filme).first()
                data = form.data.data
                horario = form.horario.data
                lotacao = request.form['lotacao']
                preco = form.preco.data
                sessao = Sessao(cine.id, film.id, data, horario.time(), lotacao, preco, 0)
                try:
                    db.session.add(sessao)
                    db.session.commit()
                    flash('Cadastro Realizado com Sucesso', 'success')
                    redirect(url_for("manage_handle"))
                except exc.IntegrityError as e:
                    print(e)
                    flash('Preencha os Campos Corretamente', 'danger')
                    db.session().rollback()
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('sessionregister.html', cines=cinemas, films=filmes, form=form)

@app.route('/admin/gerenciarsessoes/editarsessao/<id>', methods = ['GET', 'POST'])
def edita_sessao(id):
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            form = SessionForm(request.form)
            sessao = Sessao.query.filter_by(id=id).first()
            cines = Cinema.query.all()
            films = Filme.query.all()
            cine = Cinema.query.filter_by(id=sessao.cinema_id).first()
            film = Filme.query.filter_by(id=sessao.filme_id).first()
            if request.method == "POST" and form.validate():
                cinema = Cinema.query.filter_by(nome=request.form.get('cines')).first()
                filme = Filme.query.filter_by(titulo=request.form.get('films')).first()
                sessao.cinema_id = cinema.id
                sessao.filme_id = filme.id
                sessao.horario = form.horario.data.time()
                try:
                    db.session.commit()
                    flash('Alteracao Realizada com Sucesso', 'success')
                    redirect(url_for("manage_handle"))
                except exc.IntegrityError as e:
                    flash('Preencha os Campos Corretamente', 'danger')
                    db.session().rollback()
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('sessionregister.html', form=form, funcao="editar", cines=cines, films=films,
                           cinema=cine, filme=film, sessao=sessao)

@app.route('/admin/gerenciarsessoes/removesessao/<id>', methods = ['GET', 'POST'])
def remove_sessao(id):
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            Sessao.query.filter_by(id=id).delete()
            db.session.commit()
            return redirect(url_for("index"))
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('index.html')

@app.route('/admin/ranking', methods = ['GET', 'POST'])
def ranking():
    if 'admin' in session:
        admin = session['admin']
        if admin is True:
            valores = []
            cinemas = Cinema.query.order_by(Cinema.nome)
            for cinema in cinemas:
                filmes = Sessao.query.join(Cinema, Cinema.id == Sessao.cinema_id).add_columns(
                    Cinema.nome, Sessao.data).filter_by(nome=cinema.nome).join(Filme, Filme.id==Sessao.filme_id).add_columns(
                    Filme.titulo).join(Ingresso, Ingresso.sessao_id==Sessao.id).add_columns(
                    Filme.id).group_by(Filme.titulo)
                for filme in filmes:
                    qntIngressos = Ingresso.query.filter_by(filme_id=filme.id).filter_by(cinema_id=cinema.id).join(Filme, Filme.id==Ingresso.filme_id).add_columns(
                    Filme.titulo)
                    valores.append((filme[1], qntIngressos.count(), qntIngressos.group_by(Filme.titulo).all()[0][1]))
            v = sorted(valores, key=itemgetter(1), reverse=True)
        else:
            flash("É necessario ser admin para acessar!", 'danger')
            return redirect(url_for("index"))
    else:
        flash("É necessario ser admin para acessar!", 'danger')
        return redirect(url_for("index"))
    return render_template('ranking.html', valores=v)

@app.route('/compras')
def listar_compras():
    if 'logged_in' in session:
        admin = session['admin']
        if admin is True:
            flash("É necessario ser cliente para acessar!", 'danger')
            return redirect(url_for("index"))
        else:
            cliente = Cliente.query.filter_by(email=session['email']).first()
            compras = Compra.query.filter_by(cliente_id=cliente.id)
            ingressos_total = []
            for compra in compras:
                ingressos = Ingresso.query.filter_by(compra_id=compra.id).group_by(Ingresso.compra_id)
                for ingresso in ingressos:
                    ingressos_total.append(ingresso)
            filmes_total = []
            for ingresso in ingressos_total:
                filme = Filme.query.filter_by(id=ingresso.filme_id).join(Ingresso, Ingresso.filme_id == Filme.id).add_columns(
                Filme.id, Filme.titulo, Ingresso.id, Ingresso.preco).filter_by(id=ingresso.id).join(Compra, Compra.id == Ingresso.compra_id).add_columns(
                    Compra.id, Compra.quantidade, Compra.precoTotal).first()
                filmes_total.append(filme)
            print(filmes_total[0])
    else:
        flash("É necessario estar logado para comprar!", 'danger')
        return redirect(url_for("login"))
    return render_template('buying_list.html', filmes=filmes_total)

@app.route('/ingressos/<id>')
def mostrar_ingresso(id):
    if 'logged_in' in session:
        admin = session['admin']
        if admin is True:
            flash("É necessario ser cliente para acessar!", 'danger')
            return redirect(url_for("index"))
        else:
            compra = Compra.query.filter_by(id=id).join(Ingresso, Ingresso.compra_id == Compra.id).add_columns(
            Compra.id, Compra.quantidade, Compra.precoTotal, Ingresso.id).join(Filme, Ingresso.filme_id == Filme.id).add_columns(
            Filme.id, Filme.titulo).join(Cinema, Ingresso.cinema_id == Cinema.id).add_columns(
            Cinema.id, Cinema.nome).join(Sessao, Ingresso.sessao_id == Sessao.id).add_columns(
            Sessao.id, Sessao.data, Sessao.horario).first()
            print(compra)
    else:
        flash("É necessario estar logado para comprar!", 'danger')
        return redirect(url_for("login"))
    return render_template('show_ticket.html', compra=compra)
