from sqlalchemy import *
from app import db

Base = db.Model

class Admin(Base):

    __tablename__ = "admin"

    id = Column(Integer, primary_key=True)
    email = Column(String(50), index=True, unique=True, nullable=False)
    password = Column(String(20), nullable=False)

    def __init__(self, email=None, password=None):
        self.email = email
        self.password = password

class Cliente(Base):

    __tablename__ = "cliente"

    id = Column(Integer, primary_key=True)
    nome = Column(String(50), nullable=False)
    email = Column(String(50), index=True, unique=True, nullable=False)
    senha = Column(String(20), nullable=False)
    cpf = Column(String(11), index=True, unique=True, nullable=False)
    dtNasc = Column(Date)
    isEstudante = Column(Integer, nullable=False)

    def __init__(self, nome=None, email=None, senha=None, cpf=None, dtNasc=None, isEstudante=False):
        self.nome = nome
        self.email = email
        self.senha = senha
        self.cpf = cpf
        self.dtNasc = dtNasc
        self.isEstudante = isEstudante


class Cinema(Base):

    __tablename__ = "cinema"

    id = Column(Integer, primary_key=True)
    nome = Column(String(50), nullable=False)
    estado = Column(String(50), nullable=False)
    cidade = Column(String(50), nullable=False)
    localizacao = Column(String(50))

    def __init__(self, nome=None, estado=None, cidade=None, localizacao=None):
        self.nome = nome
        self.estado = estado
        self.cidade = cidade
        self.localizacao = localizacao

class Filme(Base):

    __tablename__ = "filme"

    id = Column(Integer, primary_key=True)
    titulo = Column(String(50), index=True, unique=True, nullable=False)
    sinopse = Column(String(1000))
    genero = Column(String(20), nullable=False)
    imagem = Column(String(500))
    classificacao = Column(String(3), nullable=False)
    sessoes = db.relationship('Sessao', backref='filme', lazy=True)

    def __init__(self, titulo=None, sinopse=None, genero=None, imagem=None, classificacao=None):
        self.titulo = titulo
        self.sinopse = sinopse
        self.genero = genero
        self.imagem = imagem
        self.classificacao = classificacao

class Sessao(Base):

     __tablename__ = "sessao"

     id = Column(Integer, primary_key=True)
     data = Column(String, nullable=False)
     horario = Column(Time, nullable=False)
     lotacao = Column(Integer, nullable=False)
     preco = Column(Float, nullable=False)
     isCheia = Column(Integer, nullable=False)
     cinema_id = Column(Integer, ForeignKey('cinema.id'), nullable=True)
     filme_id = Column(Integer, ForeignKey('filme.id'), nullable=False)

     def __init__(self, cinema=None, filme=None, data=None, horario=None, lotacao=None, preco=None, isCheia=False):
         self.cinema_id = cinema
         self.filme_id = filme
         self.data = data
         self.horario = horario
         self.lotacao = lotacao
         self.preco = preco
         self.isCheia = isCheia

class Compra(Base):

    __tablename__ = "compra"

    id = Column(Integer, primary_key=True)
    cliente_id = Column(Integer, ForeignKey('cinema.id'), nullable=False)
    quantidade = Column(Integer, nullable=False)
    precoTotal = Column(Float, nullable=False)

    def __init__(self, cliente=None, quantidade=None, precoTotal=None):
        self.cliente_id = cliente
        self.quantidade = quantidade
        self.precoTotal = precoTotal

class Ingresso(Base):

    __tablename__ = "ingresso"

    id = Column(Integer, primary_key=True)
    cinema_id = Column(Integer, ForeignKey('cinema.id'), nullable=False)
    filme_id = Column(Integer, ForeignKey('filme.id'), nullable=False)
    sessao_id = Column(Integer, ForeignKey('sessao.id'), nullable=False)
    compra_id = Column(Integer, ForeignKey('compra.id'), nullable=False)
    preco = Column(Float, nullable=False)

    def __init__(self, cinema=None, filme=None, sessao=None, compra=None, preco=None):
        self.cinema_id = cinema
        self.filme_id = filme
        self.sessao_id = sessao
        self.compra_id = compra
        self.preco = preco