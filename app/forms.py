from wtforms import StringField,PasswordField,BooleanField, TextAreaField,SubmitField,FileField,DateField,Form,DateTimeField,FloatField,IntegerField
from wtforms.validators import DataRequired, EqualTo, Length, Optional, Email, NumberRange
from wtforms.fields.html5 import DateField

class RegisterForm(Form):

    name = StringField(validators=[DataRequired(message="Campo obrigatorio"),
                                        Length(min=3, max=50, message='Nome deve ter entre 3 e 50 caracteres.')],
                                        render_kw={"placeholder": "Full Name"})
    email = StringField("Email", validators=[DataRequired(), Email()],
                                        render_kw= {"placeholder": "Email Address"})
    password = PasswordField("Password", validators=[DataRequired(),
                                                    EqualTo('confirm', message="Password must match!")],
                                                    render_kw={"placeholder": "Password"})
    confirm = PasswordField("Repeat Password", validators=[DataRequired()],
                                                            render_kw={"placeholder": "Confirm Password"})
    cpf = StringField("Cpf", validators=[DataRequired(),
                                        Length(min=11, max=11, message="O cpf deve ter 11 caracteres")],
                                        render_kw={"placeholder": "CPF"})
    birthday = DateField("Birthday", validators= [DataRequired()] ,render_kw={"placeholder": "Date of Birth"})
    avatar = FileField("Avatar", render_kw={"placeholder": "Full Name"})
    isStudent = BooleanField("Declaro que sou estudante")

class LoginForm(Form):

    email = StringField("Email", validators=[DataRequired(message="*Campo obrigatorio!"), Email()],
                                        render_kw= {"placeholder": "Email Address"})
    password = PasswordField("Password", validators=[DataRequired(message="*Campo obrigatorio!")],
                                                    render_kw={"placeholder": "Password"})

class CineForm(Form):

    name = StringField("Name", validators=[DataRequired(message="*Campo obrigatorio!"),
                                           Length(max=50, message="*Nome não pode passar de 50 caracteres!")],
                                           render_kw={"placeholder":"Name"})
    # state = StringField("State", validators=[DataRequired(message="*Campo obrigatorio!")],
    #                                          render_kw={"placeholder":"States"})
    city = StringField("City", validators=[DataRequired(message="*Campo obrigatorio!"),
                                            Length(max=50, message="*Nome da cidade não pode passar de 50 caracteres!")],
                                            render_kw={"placeholder":"City"})
    localization = StringField("City", validators=[DataRequired(message="*Campo obrigatorio!"),
                                            Length(max=50, message="*Localizacao não pode passar de 50 caracteres!")],
                                            render_kw={"placeholder":"Localization"})
                                        
class FilmForm(Form):

    titulo = StringField("titulo", validators=[DataRequired(message="*Campo obrigatorio!"),
                                           Length(max=50, message="*titulo não pode passar de 50 caracteres!")],
                                           render_kw={"placeholder":"titulo"})
    # sinopse = TextAreaField("sinopse", validators=[Length(max=1000, message="*Sinopse não pode passar de 1000 caracteres!")],
    #                         render_kw={"placeholder": "Sinopse"})
    genero = StringField("genero", validators=[DataRequired(message="*Campo obrigatorio!"),
                                           Length(max=50, message="*genero não pode passar de 50 caracteres!")],
                                           render_kw={"placeholder":"genero"})
    imagem  = StringField("imagem", render_kw={"placeholder":"Imagem"})
    classificacao = StringField("classificacao", validators=[DataRequired(message="*Campo obrigatorio!")],
                                           render_kw={"placeholder":"Classificacao"})

class SessionForm(Form):

    # cinema = StringField("cinema", validators=[DataRequired(message="*Campo obrigatorio!"),
    #                                                Length(max=50, message="*titulo não pode passar de 50 caracteres!")],
    #                          render_kw={"placeholder": "titulo"})
    # filme = StringField("filme", validators=[DataRequired(message="*Campo obrigatorio!"),
    #                                                Length(max=50, message="*genero não pode passar de 50 caracteres!")],
    #                          render_kw={"placeholder": "genero"})
    # horario = StringField("horario", validators=[DataRequired(message="*Campo Obrigatorio!"), Length(max=5, min=5)], format= ,render_kw={"placeholder": "Imagem"})
    data = DateField("Data", validators=[DataRequired(message="*Campo Obrigatorio")], render_kw={"placeholder": "Data"})
    horario = DateTimeField("horario", format="%H:%M", validators=[DataRequired(message="*Campo Obrigatorio!")],
                            render_kw = {"placeholder": "Horario"})
    preco = FloatField("Preço", validators=[NumberRange(min=0, max=100),
                                            DataRequired(message="*Campo obrigatorio!")])
    # preco = FloatField("Preco", validators=[NumberRange(min=0, max=1000),
    #                                         DataRequired(message="*Campo obrigatorio!")])
    # isCheia = BooleanField("ischeia", render_kw={"placeholder": "Classificacao"})

class BuyForm(Form):
    quantidade = IntegerField("Quantidade", render_kw={"placeholder": "Quantidade de ingressos"})
    #quando o clique tiver funcionando dos assentos
    #preco = FloatField("Preco", render_kw={'disabled':''})

class PasswordRecoveryForm(Form):
    email = StringField("Email", validators=[DataRequired(), Email()], render_kw= {"placeholder": "Digite seu Email"})
