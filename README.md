## SISTEMA GERÊNCIADOR DE INGRESSOS
#### PASSO A PASSO PARA RODAR O SGI
#### 1º CRIAR UM VIRTUAL ENVIRONMENT (VENV) E ABRIR O VENV
> virtualenv -p python3 venv

> source venv/bin/activate

#### 2º INSTALAR OS REQUERIMENTOS DO PROJETO
> pip install -r requirements.txt


#### 3º SETAR VARIAVEIS AMBIENTES
> export FLASK_APP=cinema.py

> export FLASK_ENV=development

> export FLASK_DEBUG=1

#### 4º INICIAR O BD (SOMENTE NA 1ª VEZ)
> flask db init

#### 5º MIGRAR E ATUALIZAR O BD(NECESSARIO SEMPRE QUE ALTERAR O BD)
> flask db migrate

> flask db upgrade

#### 6º RODAR A APLICACAO
> flask run